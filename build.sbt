val circeVersion      = "0.13.0"
val catsEffectVersion = "2.2.0"
val http4sVersion     = "0.21.8"
val maryVersion       = "5.2"
val xalanVersion      = "2.7.2"

lazy val commonSettings = Seq(
  organization := "ca.valencik",
  scalaVersion := "2.13.3",
  version := "0.1.0-SNAPSHOT"
)

lazy val tts = (project in file("modules/tts"))
  .settings(commonSettings)
  .settings(
    resolvers += "Spring Plugins" at "https://repo.spring.io/plugins-release/",
    libraryDependencies ++= Seq(
      "org.typelevel" %% "cats-effect"       % catsEffectVersion,
      "de.dfki.mary"  % "voice-cmu-slt-hsmm" % maryVersion,
      // xalan changes the XSLT parsing for the Mary voice fixing a build problem
      // https://github.com/marytts/marytts/issues/455
      "xalan" % "xalan" % xalanVersion
    )
  )

lazy val root = (project in file("modules/core"))
  .settings(commonSettings)
  .settings(
    name := "say4s",
    libraryDependencies ++= Seq(
      "io.circe"      %% "circe-core"          % circeVersion,
      "io.circe"      %% "circe-parser"        % circeVersion,
      "org.typelevel" %% "cats-effect"         % catsEffectVersion,
      "org.http4s"    %% "http4s-blaze-client" % http4sVersion
    )
  )
  .dependsOn(tts)
  .aggregate(tts)
