package businessbot

import cats.effect._

import org.http4s._
import org.http4s.client._
import org.http4s.client.dsl.Http4sClientDsl
import org.http4s.headers._
import org.http4s.multipart.{Multipart, Part}

import java.io.File

class SlackBot(channel: String)(implicit F: ContextShift[IO]) extends Http4sClientDsl[IO] {
  val token = "xoxb-put the token here!"

  def getRequest(filename: String)(blocker: Blocker): IO[Request[IO]] = {
    val multipart = Multipart[IO](
      Vector(
        Part.fileData(
          "file",
          new File(filename),
          blocker,
          `Content-Type`(MediaType.audio.wav)
        )
      )
    )

    val headers = List(
      Authorization(Credentials.Token(AuthScheme.Bearer, token)),
      Accept(MediaType.application.json)
    ) ++ multipart.headers.toList

    Method
      .POST(
        multipart,
        Uri.unsafeFromString(
          s"https://slack.com/api/files.upload?channels=${channel}&initial_comment=Attention%20employees%2C%20please%20review."
        )
      )
      .map(_.putHeaders(headers: _*))
  }

  def uploadWav(client: Client[IO], filename: String)(blocker: Blocker) =
    for {
      request <- getRequest(filename)(blocker)
      body    <- client.expect[String](request)
      _       <- IO(println(body))
    } yield ()
}
