package businessbot

import cats.effect._
import cats.implicits._

import org.http4s.client._

import io.circe._, io.circe.parser._

class Instagram(name: String, account: String) {
  case class Counts(followers: Int, following: Int, posts: Int)

  val linePrefix = """<script type="text/javascript">window._sharedData = """
  val lineSuffix = """;</script>"""

  def extractCounts(countLine: String): Option[Counts] = {
    val jsonString      = countLine.substring(linePrefix.length, countLine.length - lineSuffix.length)
    val parseResult     = parse(jsonString).getOrElse(Json.Null)
    val cursor: HCursor = parseResult.hcursor
    val user = cursor
      .downField("entry_data")
      .downField("ProfilePage")
      .downArray
      .downField("graphql")
      .downField("user")
    val followers = user.downField("edge_followed_by").downField("count").as[Int].toOption
    val follows   = user.downField("edge_follow").downField("count").as[Int].toOption
    val posts     = user.downField("edge_owner_to_timeline_media").downField("count").as[Int].toOption
    List(followers, follows, posts).sequence.map { case x => Counts(x(0), x(1), x(2)) }
  }

  def formatMessage(c: Counts): String =
    s"""|
        |On Instagram, ${name} now has a total of ${c.followers} followers.
        |They are following ${c.following} users,
        |and have made ${c.posts} posts.
        |Thank you for your time, that will be all for today.
     """.stripMargin

  def bodyToMsg(body: String): String =
    body
      .split("\n")
      .find(_.contains(linePrefix))
      .flatMap(extractCounts)
      .map(formatMessage)
      .getOrElse("Sorry Derpslack I couldn't figure it out.")

  def fetchCountMsg(client: Client[IO]) =
    for {
      body     <- client.expect[String](s"https://www.instagram.com/${account}/")
      countMsg <- IO(bodyToMsg(body))
    } yield countMsg
}
