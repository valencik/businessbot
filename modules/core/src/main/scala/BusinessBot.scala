package businessbot

import scala.util.Try
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.MILLISECONDS

import cats.effect._

import org.http4s.client._
import org.http4s.client.blaze.BlazeClientBuilder

object Greeter {
  def longToMessage(ms: Long): String =
    Try {
      val date = new java.util.Date(ms)
      val sdf  = new java.text.SimpleDateFormat("EEEE, MMMM d, HH:mm z")
      sdf.setTimeZone(java.util.TimeZone.getTimeZone("GMT-4"))
      sdf.format(date)
    }.toOption
      .map(t => s"Good Morning Derpslack, the date is ${t}.\n")
      .getOrElse("Good morning Derpslack, I have no idea what date it is.\n")

  def getDateMessage(implicit timer: Clock[IO]): IO[String] =
    for {
      unixMs      <- timer.realTime(MILLISECONDS)
      dateMessage <- IO(longToMessage(unixMs))
    } yield dateMessage
}

object BusinessBot extends IOApp {
  val httpClient: Resource[IO, Client[IO]] = BlazeClientBuilder[IO](global).resource
  val slackBot                             = new SlackBot("bottest")
  val instagram                            = new Instagram("Affinio", "affinioinc")

  def run(args: List[String]) = Blocker[IO].use { blocker =>
    httpClient.use { client =>
      for {
        timeMsg  <- Greeter.getDateMessage
        countMsg <- instagram.fetchCountMsg(client)
        msg      <- IO(timeMsg + countMsg)
        mary     <- Speaker.getMary
        audio    <- Speaker.synthAudio(mary)(msg)
        samples  <- Speaker.getSamples(audio)
        _        <- Speaker.writeWavFile(samples, "business_report.wav", audio.getFormat())
        _        <- slackBot.uploadWav(client, "business_report.wav")(blocker)
      } yield ExitCode.Success
    }
  }
}
