package businessbot

import cats.effect._

import javax.sound.sampled.AudioInputStream
import javax.sound.sampled.AudioFormat

import marytts.MaryInterface
import marytts.LocalMaryInterface
import marytts.util.data.audio.MaryAudioUtils

object Speaker {
  val getMary: IO[LocalMaryInterface] = IO(new LocalMaryInterface())

  def synthAudio(mary: MaryInterface)(text: String): IO[AudioInputStream] =
    IO(mary.generateAudio(text))

  def getSamples(audio: AudioInputStream): IO[Array[Double]] =
    IO(MaryAudioUtils.getSamplesAsDoubleArray(audio))

  def writeWavFile(samples: Array[Double], filename: String, format: AudioFormat): IO[Unit] =
    IO(MaryAudioUtils.writeWavFile(samples, filename, format))
}
